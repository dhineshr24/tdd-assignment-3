package com.company;

import java.util.Arrays;
import java.util.List;

public class LuckyNumbers
{
    // This function returns true if n is lucky
    // a number is lucky if all its digits are distinct
    public static boolean isLucky(int n)
    {
        boolean arr[]=new boolean[10];
        Arrays.fill(arr,false);

        while (n > 0)
        {
            int digit = n % 10;
            if (arr[digit])
                return false;

            arr[digit] = true;
            n /= 10;
        }
        return true;
    }

    public static void main (String[] args)
    {

        FileRepository fileRepository=new FileRepository();
        List<Integer> list=fileRepository.getFromFile("input.txt");

        list.forEach(e->
                {
                    if(isLucky(e))
                        System.out.print(e + " is Lucky \n");
                    else
                        System.out.print(e + " is Not Lucky \n");
                }
                );

    }
}


